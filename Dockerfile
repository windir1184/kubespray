ARG PYTHON_VERSION=3.12.4
FROM python:${PYTHON_VERSION}-alpine

LABEL description="Kubespray provisioner image for deploying private Kubernetes" \
      maintainer="windir1184<mirror.ashyoff@outlook.com>"

RUN apk update && \
    apk add --no-cache git \
        curl \
        openssh-client \
        yq \
        bash

WORKDIR /opt

ARG KUBESPRAY_VERSION
RUN git clone https://github.com/kubernetes-sigs/kubespray.git && \
    cd kubespray && \
    git checkout v${KUBESPRAY_VERSION} && \
    pip install -r requirements.txt && \
    pip install ruamel.yaml

# windir1184 patch
COPY inventory.py /opt/kubespray/contrib/inventory_builder/inventory.py

ENTRYPOINT [ "/bin/bash" ]